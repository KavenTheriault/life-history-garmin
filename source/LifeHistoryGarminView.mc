import Toybox.Graphics;
import Toybox.WatchUi;

class LifeHistoryGarminView extends WatchUi.View {
  public var newButton;
  private var newButtonImage;
  private var newButtonHighlightedImage;
  private var newButtonDisabledImage;

  public var resultText;

  function initialize() {
    View.initialize();
    resultText = "Life History";
  }

  // Load your resources here
  function onLayout(dc as Dc) as Void {
    newButtonImage = new WatchUi.Bitmap({
      :rezId => $.Rez.Drawables.NewButton,
    });
    newButtonHighlightedImage = new WatchUi.Bitmap({
      :rezId => $.Rez.Drawables.NewButtonHighlighted,
    });
    newButtonDisabledImage = new WatchUi.Bitmap({
      :rezId => $.Rez.Drawables.NewButtonDisabled,
    });
    var newButtonImageDim = newButtonImage.getDimensions();

    newButton = new WatchUi.Button({
      :stateDefault => newButtonImage,
      :stateHighlighted => newButtonHighlightedImage,
      :stateSelected => Graphics.COLOR_TRANSPARENT,
      :stateDisabled => newButtonDisabledImage,
      :stateHighlightedSelected => Graphics.COLOR_TRANSPARENT,
      :background => Graphics.COLOR_TRANSPARENT,
      :behavior => :onNewButtonSelected,
      :locX => dc.getWidth() / 2 - newButtonImageDim[0] / 2,
      :locY => (dc.getHeight() / 3) * 2 - newButtonImageDim[1] / 2,
      :width => newButtonImageDim[0],
      :height => newButtonImageDim[1],
    });

    setLayout([newButton]);
  }

  // Called when this View is brought to the foreground. Restore
  // the state of this View and prepare it to be shown. This includes
  // loading resources into memory.
  function onShow() as Void {}

  // Update the view
  function onUpdate(dc as Dc) as Void {
    // Call the parent onUpdate function to redraw the layout
    View.onUpdate(dc);

    dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);
    dc.drawText(
      dc.getWidth() / 2,
      dc.getHeight() / 3,
      Graphics.FONT_SMALL,
      resultText,
      Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER
    );
  }

  // Called when this View is removed from the screen. Save the
  // state of this View here. This includes freeing resources from
  // memory.
  function onHide() as Void {}
}
