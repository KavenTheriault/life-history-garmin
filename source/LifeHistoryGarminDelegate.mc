import Toybox.Communications;
import Toybox.Lang;
import Toybox.System;
import Toybox.Time;
import Toybox.Time.Gregorian;
import Toybox.WatchUi;

class LifeHistoryGarminDelegate extends WatchUi.BehaviorDelegate {
  private var _view as LifeHistoryGarminView;
  private var _creatingLifeEntry as Boolean;
  private var currentDayId;

  function initialize(view as LifeHistoryGarminView) {
    WatchUi.BehaviorDelegate.initialize();
    _view = view;
    _creatingLifeEntry = false;
  }

  public function onKey(evt as KeyEvent) as Boolean {
    var key = evt.getKey();
    if (key == KEY_ENTER && _creatingLifeEntry == false) {
      onNewButtonSelected();
      return true;
    }
    return false;
  }

  public function onNewButtonSelected() as Void {
    _view.newButton.setState(:stateDisabled);
    _creatingLifeEntry = true;

    getDay();
    updateText("Fetching day...");
  }

  function updateText(text as String) as Void {
    _view.resultText = text;
    WatchUi.requestUpdate();
  }

  function onDayAvailable(dayId as String) as Void {
    currentDayId = dayId;

    createLifeEntry();
    updateText("Creating life entry...");
  }

  function buildDateString() as String {
    var today = Gregorian.info(Time.now(), Time.FORMAT_SHORT);
    var dateString = Lang.format("$1$-$2$-$3$", [
      today.year.format("%04d"),
      today.month.format("%02d"),
      today.day.format("%02d"),
    ]);
    return dateString;
  }

  function buildTimeString() as String {
    var clockTime = System.getClockTime();
    var timeString = Lang.format("$1$:$2$:00", [
      clockTime.hour.format("%02d"),
      clockTime.min.format("%02d"),
    ]);
    return timeString;
  }

  function getDay() as Void {
    var apiClient = new $.ApiClient();
    apiClient.getDay(buildDateString(), method(:getDayCallback));
  }

  function createDay() as Void {
    var apiClient = new $.ApiClient();
    var day = { "date" => buildDateString() };
    apiClient.addDay(day, method(:createDayCallback));
  }

  function createLifeEntry() as Void {
    var apiClient = new $.ApiClient();
    var lifeEntry = {
      "day_id" => currentDayId,
      "start_time" => buildTimeString(),
    };
    apiClient.addLifeEntry(lifeEntry, method(:createLifeEntryCallback));
  }

  function getDayCallback(responseCode as Number, data as Dictionary?) as Void {
    if (responseCode == 200) {
      onDayAvailable(data["id"]);
    } else {
      createDay();
      updateText("Creating day...");
    }
  }

  function createDayCallback(
    responseCode as Number,
    data as Dictionary?
  ) as Void {
    if (responseCode == 201) {
      onDayAvailable(data["id"]);
    } else {
      updateText("Error creating day");
    }
  }

  function createLifeEntryCallback(
    responseCode as Number,
    data as Dictionary?
  ) as Void {
    if (responseCode == 201) {
      updateText("Life entry created!");

      _creatingLifeEntry = false;
      autoCloseApp();
    } else {
      updateText("Error creating life entry");
    }
  }

  function autoCloseApp() as Void {
    var myTimer = new Timer.Timer();
    myTimer.start(method(:closeApp), 5000, false);
  }

  function closeApp() as Void {
    System.exit();
}
}
