import Toybox.Lang;
import Toybox.Communications;

const SERVER_URL = "https://lifehistory.ca";

const ENDPOINTS = {
  "AUTHENTICATE" => "/api/authenticate",
  "ACTIVITY_TYPES" => "/api/activity_types",
  "ACTIVITIES" => "/api/activities",
  "DAYS" => "/api/days",
  "LIFE_ENTRIES" => "/api/life_entries",
  "LIFE_ENTRY_ACTIVITIES" => "/api/life_entry_activities",
};

class ApiClient {
  private function apiCall(
    method as Communications.HttpRequestMethod,
    path as String,
    data as Lang.Dictionary?,
    responseCallback
  ) as Void {
    var url = SERVER_URL + path;
    var options = {
      :method => method,
      :headers => {
        "Content-Type" => Communications.REQUEST_CONTENT_TYPE_JSON,
        "Authorization" => WatchUi.loadResource(
          $.Rez.Strings.AuthorizationHeader
        ),
      },
      :responseType => Communications.HTTP_RESPONSE_CONTENT_TYPE_JSON,
    };

    Communications.makeWebRequest(url, data, options, responseCallback);
  }

  // Days
  function getDay(dateString as String, responseCallback) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_GET,
      ENDPOINTS["DAYS"] + "/" + dateString,
      null,
      responseCallback
    );
  }
  function getDayById(id as Number, responseCallback) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_GET,
      ENDPOINTS["DAYS"] + "/" + id,
      null,
      responseCallback
    );
  }
  function addDay(day as Lang.Dictionary, responseCallback) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_POST,
      ENDPOINTS["DAYS"],
      day,
      responseCallback
    );
  }
  function updateDay(day as Lang.Dictionary, responseCallback) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_PUT,
      ENDPOINTS["DAYS"] + "/" + day["id"],
      null,
      responseCallback
    );
  }
  // Activity Types
  function getActivityTypes(responseCallback) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_GET,
      ENDPOINTS["ACTIVITY_TYPES"],
      null,
      responseCallback
    );
  }
  function getActivityType(id as Number, responseCallback) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_GET,
      ENDPOINTS["ACTIVITY_TYPES"] + "/" + id,
      null,
      responseCallback
    );
  }
  function addActivityType(
    activityType as Lang.Dictionary,
    responseCallback
  ) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_POST,
      ENDPOINTS["ACTIVITY_TYPES"],
      activityType,
      responseCallback
    );
  }
  function updateActivityType(
    activityType as Lang.Dictionary,
    responseCallback
  ) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_PUT,
      ENDPOINTS["ACTIVITY_TYPES"] + "/" + activityType["id"],
      activityType,
      responseCallback
    );
  }
  function deleteActivityType(
    activityType as Lang.Dictionary,
    responseCallback
  ) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_DELETE,
      ENDPOINTS["ACTIVITY_TYPES"] + "/" + activityType["id"],
      null,
      responseCallback
    );
  }
  function searchActivityTypes(term as String, responseCallback) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_GET,
      ENDPOINTS["ACTIVITY_TYPES"] + "/search/" + term,
      null,
      responseCallback
    );
  }
  // Activities
  function getActivities(responseCallback) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_GET,
      ENDPOINTS["ACTIVITIES"],
      null,
      responseCallback
    );
  }
  function getActivity(id as Number, responseCallback) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_GET,
      ENDPOINTS["ACTIVITIES"] + "/" + id,
      null,
      responseCallback
    );
  }
  function addActivity(activity as Lang.Dictionary, responseCallback) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_POST,
      ENDPOINTS["ACTIVITIES"],
      activity,
      responseCallback
    );
  }
  function updateActivity(
    activity as Lang.Dictionary,
    responseCallback
  ) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_PUT,
      ENDPOINTS["ACTIVITIES"] + "/" + activity["id"],
      activity,
      responseCallback
    );
  }
  function deleteActivity(
    activity as Lang.Dictionary,
    responseCallback
  ) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_DELETE,
      ENDPOINTS["ACTIVITIES"] + "/" + activity["id"],
      null,
      responseCallback
    );
  }
  function replaceActivity(
    activity as Lang.Dictionary,
    replaceActivityPayload as Lang.Dictionary,
    responseCallback
  ) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_POST,
      ENDPOINTS["ACTIVITIES"] + "/" + activity["id"] + "/replace",
      replaceActivityPayload,
      responseCallback
    );
  }
  function searchActivities(term as String, responseCallback) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_GET,
      ENDPOINTS["ACTIVITIES"] + "/search/" + term,
      null,
      responseCallback
    );
  }
  function getActivityDescriptions(
    activity as Lang.Dictionary,
    filter as String,
    responseCallback
  ) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_GET,
      ENDPOINTS["ACTIVITIES"] + "/" + activity["id"] + "/descriptions",
      { "filter" => filter },
      responseCallback
    );
  }
  function getActivitiesBestOf(
    query as Lang.Dictionary,
    responseCallback
  ) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_POST,
      ENDPOINTS["ACTIVITIES"] + "/best_of",
      query,
      responseCallback
    );
  }
  // Life Entries
  function addLifeEntry(
    lifeEntry as Lang.Dictionary,
    responseCallback
  ) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_POST,
      ENDPOINTS["LIFE_ENTRIES"],
      lifeEntry,
      responseCallback
    );
  }
  function getLifeEntry(id as Number, responseCallback) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_GET,
      ENDPOINTS["LIFE_ENTRIES"] + "/" + id,
      null,
      responseCallback
    );
  }
  function updateLifeEntry(
    lifeEntry as Lang.Dictionary,
    responseCallback
  ) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_PUT,
      ENDPOINTS["LIFE_ENTRIES"] + "/" + lifeEntry["id"],
      lifeEntry,
      responseCallback
    );
  }
  function deleteLifeEntry(
    lifeEntry as Lang.Dictionary,
    responseCallback
  ) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_DELETE,
      ENDPOINTS["LIFE_ENTRIES"] + "/" + lifeEntry["id"],
      null,
      responseCallback
    );
  }
  // Life Entry Activities
  function addLifeEntryActivity(
    lifeEntryActivity as Lang.Dictionary,
    responseCallback
  ) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_POST,
      ENDPOINTS["LIFE_ENTRY_ACTIVITIES"],
      lifeEntryActivity,
      responseCallback
    );
  }
  function getLifeEntryActivity(id as Number, responseCallback) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_GET,
      ENDPOINTS["LIFE_ENTRY_ACTIVITIES"] + "/" + id,
      null,
      responseCallback
    );
  }
  function updateLifeEntryActivity(
    lifeEntryActivity as Lang.Dictionary,
    responseCallback
  ) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_PUT,
      ENDPOINTS["LIFE_ENTRY_ACTIVITIES"] + "/" + lifeEntryActivity["id"],
      lifeEntryActivity,
      responseCallback
    );
  }
  function deleteLifeEntryActivity(
    lifeEntryActivity as Lang.Dictionary,
    responseCallback
  ) as Void {
    apiCall(
      Communications.HTTP_REQUEST_METHOD_DELETE,
      ENDPOINTS["LIFE_ENTRY_ACTIVITIES"] + "/" + lifeEntryActivity["id"],
      null,
      responseCallback
    );
  }
}
